<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orderdetail */

$this->title = 'Update Orderdetail: ' . $model->order_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Orderdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_detail_id, 'url' => ['view', 'id' => $model->order_detail_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="orderdetail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

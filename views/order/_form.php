<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>



<?php


use yii\bootstrap\ActiveForm;

use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */

/* @var $modelCustomer app\modules\yii2extensions\models\Customer */

/* @var $modelsAddress app\modules\yii2extensions\models\Address */


$js = '

jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Order Detail: " + (index + 1))

    });

});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Order Detail: " + (index + 1))

    });

});

';


$this->registerJs($js);


?>


<div class="customer-form">


    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="row">

        <div class="col-sm-6">

            <?= $form->field($modelOrder, 'customer_id')->textInput(['maxlength' => true]) ?>

        </div>

        <div class="col-sm-6">

            <?= $form->field($modelOrder, 'description')->textInput(['maxlength' => true]) ?>

        </div>

    </div>


    <div class="padding-v-md">

        <div class="line line-dashed"></div>

    </div>

    <?php DynamicFormWidget::begin([

        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]

        'widgetBody' => '.container-items', // required: css class selector

        'widgetItem' => '.item', // required: css class

        'limit' => 4, // the maximum times, an element can be cloned (default 999)

        'min' => 0, // 0 or 1 (default 1)

        'insertButton' => '.add-item', // css class

        'deleteButton' => '.remove-item', // css class

        'model' => $modelsOrderdetail[0],

        'formId' => 'dynamic-form',

        'formFields' => [

            'quantity',

            'shipping_address',

            'shipping_city',

        ],

    ]); ?>

    <div class="panel panel-default">

        <div class="panel-heading">

            <i class="fa fa-envelope"></i> Order Detail

            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Order Detail</button>

            <div class="clearfix"></div>

        </div>

        <div class="panel-body container-items"><!-- widgetContainer -->

            <?php foreach ($modelsOrderdetail as $index => $modelOrderdetail): ?>

                <div class="item panel panel-default"><!-- widgetBody -->

                    <div class="panel-heading">

                        <span class="panel-title-address">Order Detail: <?= ($index + 1) ?></span>

                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>

                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

                        <?php

                            // necessary for update action.

                            if (!$modelOrderdetail->isNewRecord) {

                                echo Html::activeHiddenInput($modelOrderdetail, "[{$index}]id");

                            }

                        ?>

                        <?= $form->field($modelOrderdetail, "[{$index}]quantity")->textInput(['maxlength' => true]) ?>


                        <div class="row">

                            <div class="col-sm-6">

                                <?= $form->field($modelOrderdetail, "[{$index}]shipping_address")->textInput(['maxlength' => true]) ?>

                            </div>

                            <div class="col-sm-6">

                                <?= $form->field($modelOrderdetail, "[{$index}]shipping_city")->textInput(['maxlength' => true]) ?>

                            </div>

                        </div><!-- end:row -->

                    </div>

                </div>

            <?php endforeach; ?>

        </div>

    </div>

    <?php DynamicFormWidget::end(); ?>


    <div class="form-group">

        <?= Html::submitButton($modelOrderdetail->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>

    </div>


    <?php ActiveForm::end(); ?>


</div>
<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orderdetail;

/**
 * OrderdetailSearch represents the model behind the search form about `app\models\Orderdetail`.
 */
class OrderdetailSearch extends Orderdetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_detail_id', 'order_id', 'product_id', 'quantity'], 'integer'],
            [['shipping_address', 'shipping_city', 'date_created_datetime', 'date_updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orderdetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_detail_id' => $this->order_detail_id,
            'order_id' => $this->order_id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'date_created_datetime' => $this->date_created_datetime,
            'date_updated_datetime' => $this->date_updated_datetime,
        ]);

        $query->andFilterWhere(['like', 'shipping_address', $this->shipping_address])
            ->andFilterWhere(['like', 'shipping_city', $this->shipping_city]);

        return $dataProvider;
    }
}

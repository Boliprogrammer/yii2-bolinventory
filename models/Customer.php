<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $customer_id
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $phone
 * @property string $nombre_facturacion
 * @property string $nit_facturacion
 * @property string $date_created_datetime
 * @property string $date_updated_datetime
 *
 * @property Order[] $orders
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created_datetime', 'date_updated_datetime'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 100],
            [['address', 'nombre_facturacion'], 'string', 'max' => 200],
            [['phone', 'nit_facturacion'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'nombre_facturacion' => 'Nombre Facturacion',
            'nit_facturacion' => 'Nit Facturacion',
            'date_created_datetime' => 'Date Created Datetime',
            'date_updated_datetime' => 'Date Updated Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @inheritdoc
     * @return CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerQuery(get_called_class());
    }
}
